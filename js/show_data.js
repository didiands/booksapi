
function showData(cbox)
 {
   // This function displays a related textbox when a checkbox is checked
   
   // If the checkbox is checked,
   if(cbox.checked)
     {
       // display the related textbox
       document.getElementById(cbox.name).style.display = 'block';  
     }
   // Otherwise,
   else
     {
       // don't display it
       document.getElementById(cbox.name).style.display = 'none';
       document.getElementById(cbox.name).childNodes[3].value = '';
     }
 }

