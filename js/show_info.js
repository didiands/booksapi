
// Specify the user info for each textbox
var age_group_info = 'Specify an age group for the book';
var author_info = 'Specify an author for the book';
var contributor_info = 'Specify any possible contributor for the book';
var isbn_info = 'Specify an ISBN for the book';
var price_info = 'Specify a price for the book, the price must be positive';
var publisher_info = 'Specify a publisher for the book';
var title_info = 'Specify a title for the book';

/* 
  Create an array for the info of each search criterion 
  Each criterion is represented by an object which has:
   * A name
   * The info related to it
   * The id of the paragraph in which the info will be displayed
   * The id of the div element in which all the contents related to the
     search criterion are located
*/
var fields_info = [
{name:'age_group',info:age_group_info, p_id:'age_group_p', d_id:'age_group_c'}, 
{name:'author', info:author_info, p_id:'author_p', d_id:'author_c'},
{name:'contributor', info:contributor_info, p_id:'contributor_p', d_id:'contributor_c'},
{name:'isbn', info:isbn_info, p_id:'isbn_p', d_id:'isbn_c'},
{name:'price', info:price_info, p_id:'price_p', d_id:'price_c'},
{name:'publisher', info:publisher_info, p_id:'publisher_p', d_id:'publisher_c'},
{name:'title', info:title_info, p_id:'title_p', d_id:'title_c'}];


function showInfo(textbox)
  {
    // This function displays the info of a textbox
    
    // Search in the array,
    for(var i = 0; i < fields_info.length; i++)
      {
        // If the name of the textbox is found
        if (textbox.name === fields_info[i].name)
          {
            // Create a paragraph element
            var p = document.createElement('P');
            
            // Set the id of the paragraph
            p.id = fields_info[i].p_id;
            
            // Set the criterion info as the node of the paragraph
            var t = document.createTextNode(fields_info[i].info);
            
            // Append the node to the paragraph
            p.appendChild(t);
            
            // Append the paragraph to the div of the criterion
            document.getElementById(fields_info[i].d_id).appendChild(p);
          }
      }
  }

function hideInfo(textbox)
  {
    // This function hides the info of a textbox
    
    // Loop into the array of criteria
    for(var i = 0; i < fields_info.length; i++)
      {
        // If the name of the textbox matches the name of a criterion,
        if (textbox.name === fields_info[i].name)
          {
            // Delete the info located in the paragraph of the textbox
            document.getElementById(fields_info[i].p_id).remove();
          }
      }
  }
