
function displayResults()
            {
               // Get values from textboxes
               age_group = document.getElementsByName('age_group')[0].value;
               author = document.getElementsByName('author')[0].value;
               contributor = document.getElementsByName('contributor')[0].value;
               isbn = document.getElementsByName('isbn')[0].value;
               price = document.getElementsByName('price')[0].value;
               publisher = document.getElementsByName('publisher')[0].value;
               title = document.getElementsByName('title')[0].value;
               
               // If there is no data in all the textbox, don't display anything
               if (age_group === '' && author === '' && contributor === ''
                   && isbn === '' && price === '' && publisher === '' && title === '')
                 {
                   document.getElementById('search_results').innerHTML = '';
                 }
               
               // Otherwise, 
               else
                 {
                   // If the price is negative,
                   if (price < 0)
                     {
                       // Let the user know
                       window.alert("Sorry, the price has to be positive");
      
                       // Reset the value of the price
                       document.getElementsByName('price')[0].value = '';
      
                       // Cause the cursor to keep blinking in the price field
                       document.getElementsByName('price')[0].focus();
                     }
                   // otherwise,
                   else
                     {
                       // Create a variable to hold the request object
                       var xhttp;
                       
                       // If the XMLHttpRequest object is supported by the browser,
                       if(window.XMLHttpRequest)
                         {
                           // Create it,
                           xhttp = new XMLHttpRequest();
                         }
                       // otherwise,
                       else
                         {
                           // Create the appropriate object
                           xhttp = new ActiveXObject("Microsoft.XMLHTTP");
                         }
                         
                       // specify the request,
                       xhttp.open("POST", "results.php", true);
                       xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                       
                       // build the query
                       str = 'age_group=' + age_group
                           + '&author=' + author
                           + '&contributor=' + contributor
                           + '&isbn=' + isbn
                           + '&price=' + price
                           + '&publisher=' + publisher
                           + '&title=' + title;
                       // send it to the server
                       xhttp.send(str);
                       
                       // Everytime the state of the request changes,
                       xhttp.onreadystatechange = function(){
                       
                       // If a result is successfully received,
                       if(this.readyState === 4 && this.status === 200)
                         {
                           // Display the result in the div
                           document.getElementById('search_results').innerHTML = this.responseText;
                         }};
                     }
                 }      
            }
                
              
       