<!DOCTYPE html>

<?php

function displayBookData($result)
    {
      /* 
        This function displays a new line containing the book's title,
        description, contributor, author, price, and publisher
      */ 
       echo '<tr>';
                    
       echo '<td>'.$result->title.'</td>' 
           .'<td>'.$result->description.'</td>' 
           .'<td>'.$result->contributor.'</td>'
           .'<td>'.$result->author.'</td>'
           .'<td>'.$result->price.'</td>'
           .'<td>'.$result->publisher.'</td>';
                
       echo '</tr>';
    }
    
// Receive data from index.php
$age_group = filter_input(INPUT_POST, "age_group");
$author = filter_input(INPUT_POST, "author");
$contributor = filter_input(INPUT_POST, "contributor");
$isbn = filter_input(INPUT_POST, "isbn");
$price = filter_input(INPUT_POST, "price");
$publisher = filter_input(INPUT_POST, "publisher");
$title = filter_input(INPUT_POST, "title");

// Declare some variables to help in sorting the price
$price_to_sort = 0;
$price_given = false;

// If the price is given, save it and set it to the empty set
if($price != '')
  {
    $price_to_sort = $price;
    $price = '';
    $price_given = true;
  }
  
// Create an array for the input data
$input_data = array($age_group, $author, $contributor, $isbn, $price
                    , $publisher, $title);

// Create an array for the labels
$label_data = array('Age group: ', 'Author: ', 'Contributor: ', 'ISBN: '
                    , 'Price: ', 'Publisher: ', 'Title: ');

// Create an array for the query
$query_array = array('age-group', 'author', 'contributor', 'isbn', 'price'
                     , 'publisher', 'title');

// Create Curl resource
$curl = curl_init();

// Prepare query
$query = array("api-key" => "80c891d1ffe849af8c4b0fce8d17e9de");

// Add additional queries if the textbox is not empty
for ($i = 0; $i < count($input_data); $i++)
    {
      if($input_data[$i] != '')
        {
          $query[$query_array[$i]] = $input_data[$i];
        } 
    }
    
// Set the URL
curl_setopt($curl, CURLOPT_URL,'https://api.nytimes.com/svc/books/v3/lists/'
            . 'best-sellers/history.json' . '?' . http_build_query($query));

// Return the transfer as a string
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

// Decode the JSON file
$output = json_decode(curl_exec($curl));

// Close Curl resource to free up system resources
curl_close($curl);

?>

<html>
	<head>
		<meta charset = "utf-8"/>
                <meta name="viewport" content="width=device-width, initial-scale=1">
		<title> Results </title>
                <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
                <link href="../NYTimesBooksAPI/css/results.css" rel="stylesheet">
	</head>
	
        <body>
            
           <div>
               
            <?php
             // If no result is returned, exit the program
             if($output->num_results === 0)
                {
                  echo '<img src="../NYTimesBooksAPI/img/2.jpg" alt="sorry">';
                  echo '<br> <br>';
                  echo '<span class = "no_result">'
                       .'Sorry, your query returned no result'
                       .'</span> <br> <br>';
                  echo '<a href="../NYTimesBooksAPI/index.php">'
                       .'Try Again </a>';
                  exit();
                }
            ?>
               
           </div>
            
            <h1> Results </h1>
            
            <header>
                <?php 
                     for($i = 0; $i < count($input_data); $i++)
                       {
                         if($input_data[$i] != '')
                           {
                             echo '<label>'.$label_data[$i].'</label>'
                                  .'<span class="label_value">'.' '
                                  .$input_data[$i].'</span>'.'<br>';
                           }
                       }
                       
                     if($price_to_sort != 0)
                       {
                         echo '<label> Price: </label>'
                         .'<span class="label_value">'.' '
                         .$price_to_sort.'</span>';
                       }
                ?>
            </header>
            
            <table class="table table-hover table-bordered table-condensed">
                <caption> <?php echo $output->copyright; ?> </caption>
                <tr>
                    <th> Title </th>
                    <th> Description </th>
                    <th> Contributor </th>
                    <th> Author </th>
                    <th> Price </th>
                    <th> Publisher </th>
                </tr>
            
            <?php
                // For each result,
                foreach($output->results as $result)
                  {
                    /* 
                       If the price is given and the book's price is less than
                       the price given
                     */
                    if(($result->price <= $price_to_sort)&&($price_given))
                      {
                        displayBookData($result);
                      }
                    
                    // If the price is not given
                    else if(!$price_given)
                      {
                        displayBookData($result); 
                      }
                  }
            ?>
              
            </table>
	</body>
</html>
          
