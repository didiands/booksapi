<!DOCTYPE html>

<html>
	<head>
          <meta charset = "utf-8"/>
          <meta name="viewport" content="width=device-width, initial-scale=1">
	  <title> New York Times Bestseller </title>
          <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
          <link rel="stylesheet" href="../NYTimesBooksAPI/css/index.css">
          <script type="text/javascript" src="js/display_results.js"> </script> 
	</head>
	
        <body>
           
           <div class="all">
               
            <img src="../NYTimesBooksAPI/img/1.jpg" alt="NYTimes bestsellers books">
            
            <header> New York Times Bestsellers Books Search </header>
            
            <br>
            
              <form>
               
               <aside>
                 <h3> Search Criteria </h3>
                  
                 <label> Age group </label>
                 <input type="checkbox" name="age_group_c" class="cbox" onclick="showData(this);" checked />
                
                 <br>
                    
                 <label> Author </label>
                 <input type="checkbox" name="author_c" class="cbox" onclick="showData(this);" /> 
                 
                 <br>
                    
                 <label> Contributor </label>
                 <input type="checkbox" name="contributor_c" class="cbox" onclick="showData(this);" /> 
                 
                 <br>
                    
                 <label> ISBN </label>
                 <input type="checkbox" name="isbn_c" class="cbox" onclick="showData(this);" /> 
                 
                 <br>   
                      
                 <label> Price </label>
                 <input type="checkbox" name="price_c" class="cbox" onclick="showData(this);" /> 
                 
                 <br>
                    
                 <label> Publisher </label>
                 <input type="checkbox" name="publisher_c" class="cbox" onclick="showData(this);" /> 
                 
                 <br>
                    
                 <label> Title </label>
                 <input type="checkbox" name="title_c" class="cbox" onclick="showData(this);" /> 
                 
                 <br>
                 
                 <script type="text/javascript" src="js/show_data.js"> </script>
               </aside>
               
               <section>
                 <h3> Search By </h3>  
                 <div id="age_group_c">   
                    <label> Age group </label>
                    <input type="text" name="age_group" onmouseover="showInfo(this);" 
                           onmouseout="hideInfo(this);" onkeyup="displayResults();">
                 </div>
               
                 <div id="author_c">
                    <label> Author </label>
                    <input type="text" name="author" onmouseover="showInfo(this);" 
                           onmouseout="hideInfo(this);" onkeyup="displayResults();">
                 </div>
               
                 <div id="contributor_c">
                    <label> Contributor </label>
                    <input type="text" name="contributor" onmouseover="showInfo(this);" 
                           onmouseout="hideInfo(this);" onkeyup="displayResults();">
                 </div>
               
                 <div id="isbn_c">
                    <label> ISBN </label>
                    <input type="text" name="isbn" onmouseover="showInfo(this);" 
                           onmouseout="hideInfo(this);" onkeyup="displayResults();">
                 </div>
               
                 <div id="price_c">
                    <label> Price </label>
                    <input type="text" id="price" name="price" onmouseover="showInfo(this);" 
                           onmouseout="hideInfo(this);" onkeyup="displayResults();">
                 </div>
               
                 <script type="text/javascript" src="js/price_validator.js"></script>
               
                 <div id="publisher_c">
                    <label> Publisher </label>
                    <input type="text" name="publisher" onmouseover="showInfo(this);" 
                           onmouseout="hideInfo(this);" onkeyup="displayResults();">
                 </div>
               
                 <div id="title_c">
                    <label> Title </label>
                    <input type="text" name="title" onmouseover="showInfo(this);" 
                           onmouseout="hideInfo(this);" onkeyup="displayResults();">
                 </div>
               
                 <script type="text/javascript" src="js/show_info.js"> </script>
               
               </section>
               
            </form>
            
            <script type="text/javascript" src="js/hide_data.js"></script>
           
           </div>
            
           <div id="search_results">
                
           </div>
            
         
           
	</body>
        
</html>